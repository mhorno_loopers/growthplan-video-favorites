import {
  BrowserRouter,
  Routes,
  Route
} from "react-router-dom";
import App from "App";
import List from "components/List";
import Detail from "components/Detail";
import About from "components/About";

const Root = () => (
	<BrowserRouter basename={process.env.PUBLIC_URL || `/growthplan-video-favorites`}>
		<Routes>
			<Route path="/" element={<App />}>
				<Route path="list" element={<List />}>
					
				</Route>
				<Route path="list/:videoId" element={<Detail/>}/>
				{/* <Route path="rickandmorty" element={<RickAndMorty />} /> */}
				<Route path="/about" element={<About />}></Route>
			</Route>
			<Route
				path="*"
				element={
					<main style={{ padding: "1rem" }}>
					<p>There's nothing here!</p>
					</main>
				}
				/>
		</Routes>
	</BrowserRouter>
);


export default Root;