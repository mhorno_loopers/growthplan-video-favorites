import React from 'react';


const Header = ({onClickAdd}) => (
    <div className="header-content">
        <div className="header-title-text">Vídeos favoritos</div>
        <input type="button" onClick={onClickAdd} value="Añadir Vídeo" className="header-button-add"/>
    </div>
)



export default Header;