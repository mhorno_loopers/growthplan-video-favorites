import React from "react";

const About = () => (
    <div>
        <h1>Propósito de la página</h1>
        <h3>Este es el propósito de la página por favor leer atentamente:</h3>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi ac arcu lorem. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Sed a lectus dui. Ut at est ante. Mauris fermentum ante eget risus blandit consequat in iaculis augue. Fusce vel nunc porttitor, aliquet quam vitae, pretium enim. Curabitur viverra urna eu blandit pretium. Nulla non imperdiet libero. Quisque rutrum, diam id accumsan lobortis, enim lectus faucibus lorem, sit amet pharetra ex lorem ac sapien. Praesent condimentum massa non ex varius, a scelerisque tortor pellentesque. Ut accumsan augue at erat congue, in venenatis lectus eleifend. Nam ullamcorper quam eu est molestie, vel aliquam massa volutpat.</p>
    </div>
);

export default About;