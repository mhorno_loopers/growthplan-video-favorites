import { useParams } from "react-router-dom";
import React, {useEffect, useState} from 'react';
import { findById } from "api";
import Video from "./Video";
import Loading from "./Loading";

export default function Detail() {
  let params = useParams();
  // Declare a new state variable, which we'll call "count"
  const [isLoading, setIsLoading] = useState(false);
  const [video, setVideo] = useState({});
  const [error, setError] = useState('');

  useEffect(() => {
    setIsLoading(true);
    findById(params.videoId)
    .then(data=>{
        setVideo(data);
        setIsLoading(false);
    })
    .catch(error=>{
        setError(error);
        setIsLoading(false);
    })
  }, [params.videoId]);

  if (error) return <p className="error">{error.message}</p>;
  if (isLoading) return (<Loading message="Loading video.."/>)


  return (
      <div className="detail-container">
          <Video title={video.title} embed={video.embed} />
          <div className="detail-summary">
              <h2 className="detail-title">{video.title}</h2>
              <p>{video.description}</p>
          </div>
      </div>
  );


}