import React, { Component } from 'react';
import {getRickAndMortyCharacters} from '../api'
import Item from './Item';

class RickAndMorty extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: false,
            characters: [],
            error: ''
        }
    }

    async componentDidMount() {
        this.setState({characters: [], isLoading: true})
        const characters = await getRickAndMortyCharacters()

        this.setState({characters: characters, isLoading: false})
    }

    render() {
        const {characters} = this.state;
        return (
            <div className="container">
                <div className="grid-container">
                    {
                        characters.map((char, i) => {
                            return (<Item key={i} data={char} />)
                        })
                    }
                </div>
            </div>
        );
    }
}

export default RickAndMorty;