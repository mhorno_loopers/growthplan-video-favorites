import React, { Component } from 'react';
import Header from './Header';

import Item from './Item';
import Loading from './Loading';

import {getVideos} from '../api';
import { Outlet } from 'react-router';
import Add from './Add';

class List extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: false,
            videos: [],
            error: '',
            showAdd: false
        };
        this.handleAdd = this.handleAdd.bind(this);
        this.handleCloseAdd = this.handleCloseAdd.bind(this);
    }

    componentDidMount() {
        this.setState({ isLoading: true })
        getVideos()
            .then(response => this.setState({videos: response.data, isLoading:false}))
            .catch(error => this.setState({error, isLoading: false}))
    }

    handleAdd(e) {
        e.preventDefault();
        this.setState({showAdd:true})
    }

    handleCloseAdd(reload) {
        return () => {
            if(reload) {
                this.setState({isLoading:true, showAdd:false});
                getVideos()
                .then(response => this.setState({videos: response.data, isLoading:false}))
                .catch(error => this.setState({error, isLoading: false, showAdd:false}))
                // this.setState({isLoading:true, showAdd:false});
                // getVideos().then(data => this
                //     .setState({ videos: data, isLoading: false, showAdd: false}))
                //     .catch(error => this.setState({error, isLoading:false, showAdd:false}));
            } else {
                this.setState({showAdd:false});
            }
        }
    }

    render() {
        const { isLoading, videos, error } = this.state;
        if (error) return (<div>ERROR</div>);
        if (isLoading) return (<Loading message="Loading.." />);
        return (<React.Fragment>
            <Header onClickAdd={this.handleAdd} />
            <div className="container">
                <div className="grid-container">
                    {
                        videos.map((video, i) => {
                            return (<Item key={i} data={video} />)
                        })
                    }
                </div>
            </div>
            {this.state.showAdd && (<Add onClose={this.handleCloseAdd}/>)}
            <Outlet/>
        </React.Fragment>)
    }
}

export default List