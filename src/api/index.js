const FAKE_DELAY = 0;
const FAKE_DATA_KEY = 'MY_FAVORITE_VIDEOS';
let MY_FAKE_DATA = [
	{	
		id:0,
		title:'Getting started with AWS Lambda and Serverless Framework',
		url:'https://www.youtube.com/watch?v=JL_7Odb7GLM',
		embed:'https://www.youtube.com/embed/JL_7Odb7GLM',
		thumbnail:'https://img.youtube.com/vi/JL_7Odb7GLM/maxresdefault.jpg',
	},
	{ 	
		id:1,
		title:'AWS EC2 vs ECS vs Lambda | Which is right for YOU?',
		url: 'https://www.youtube.com/watch?v=-L6g9J9_zB8',
		embed: 'https://www.youtube.com/embed/-L6g9J9_zB8',
		thumbnail: 'https://img.youtube.com/vi/-L6g9J9_zB8/maxresdefault.jpg',
	},
	{ 
		id:2,
		title:'AWS Aurora VS DynamoDB',
		url: 'https://www.youtube.com/watch?v=crHwekf0gTA',
		embed: 'https://www.youtube.com/embed/crHwekf0gTA',
		thumbnail: 'https://img.youtube.com/vi/crHwekf0gTA/maxresdefault.jpg',
	},
	{ 	
		id:3,
		title:'Integrate your REST API with AWS Services using API Gateway Service Proxy',
		url: 'https://www.youtube.com/watch?v=i5NEHwFeeuY',
		embed: 'https://www.youtube.com/embed/i5NEHwFeeuY',
		thumbnail: 'https://img.youtube.com/vi/i5NEHwFeeuY/maxresdefault.jpg',
	},
	{ 
		id:4,
		title:'Cuando tu Startup Ya No Es una Startup',
		url: 'https://www.youtube.com/watch?v=RuMqqLkK2q0',
		embed: 'https://www.youtube.com/embed/RuMqqLkK2q0',
		thumbnail: 'https://img.youtube.com/vi/RuMqqLkK2q0/maxresdefault.jpg',
	},
	{ 
		id:5,
		title:'Phil Collins - You\'ll Be In My Heart',
		url: 'https://www.youtube.com/watch?v=w0ZHlp6atUQ',
		embed: 'https://www.youtube.com/embed/w0ZHlp6atUQ',
		thumbnail: 'https://img.youtube.com/vi/w0ZHlp6atUQ/maxresdefault.jpg',
	},
	{ 
		id:6,
		title:'How to configure your own Gitlab CI Runner',
		url: 'https://www.youtube.com/watch?v=G8ZONHOTAQk',
		embed: 'https://www.youtube.com/embed/G8ZONHOTAQk',
		thumbnail: 'https://img.youtube.com/vi/G8ZONHOTAQk/maxresdefault.jpg',
	}

]; 

function getFavStorageVideos(){
	const fakeDataStorage = localStorage.getItem(FAKE_DATA_KEY);
	let videos
	if(fakeDataStorage){
		videos = (JSON.parse(fakeDataStorage)).data;
	} else {
		videos = MY_FAKE_DATA;
	}
	return videos;
}

function persistFavVideos(data){
	localStorage.setItem(FAKE_DATA_KEY, JSON.stringify({data}));
}

export const createVideo = (video) => new Promise((resolve,reject) => {
    const videos = getFavStorageVideos();
	setTimeout(() => {
        video.id = videos.length
        videos.push(video)
		persistFavVideos(videos)
        resolve({status:201})
    }, FAKE_DELAY);
})

export const getVideos = () => new Promise((resolve, reject) => {
    setTimeout(() => {
		const videos = getFavStorageVideos();
        resolve({status:200, data: videos})
    }, FAKE_DELAY);
})

export const findById = (videoId) => new Promise((resolve, reject)=>{
	setTimeout(() => {
		try {
			const videos = getFavStorageVideos()
			const video = videos[videoId]
			fetch('https://baconipsum.com/api/?type=all-meat&paras=3&start-with-lorem=1').then(response=>response.json()).then(description=>{
				video.description = description;
				resolve(video)
			})
		}catch (error) {reject({ status: 404, error }) }

	}, FAKE_DELAY);

})

export const getRickAndMortyCharacters = async () => {
    try {
        const data = await (await fetch('https://rickandmortyapi.com/api/character/')).json()
        const characters = data.results.map(char => {
            const fChar = {...char}
            fChar.thumbnail = char.image
            fChar.title = char.name
            return fChar
        })
        return characters
    } catch (error) {console.error(error);}
}

