import './App.css';
import Footer from 'components/Footer';
import React from 'react';
import { Outlet, Link } from 'react-router-dom';

function App() {
  return (
    <div className='page-container'>
      <div className='content-wrap'>
      <nav>
        <Link className="my-link" to="/list">Listado Favoritos</Link> |{' '}
        {/* <Link to="/rickandmorty">Rickandmorty</Link> |{' '} */}
        <Link className='my-link' to="/about">About</Link> |{' '}
      </nav>
        <Outlet/>
      </div>
      <Footer></Footer>
    </div>
  );
}

export default App;
